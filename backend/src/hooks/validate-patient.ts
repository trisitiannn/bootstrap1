// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { app, data } = context;
    
    if(data.lowPressure > data.highPressure){
      throw new Error("You low systolic bp is higher then your high systolic bp");
    }
    
    
    const patient = await app.service('patients').find({
      query: {
        email : data.email
      }
    });
    
    // Throw an error if this is a duplicate plate
    if(patient.total > 0) {
      throw new Error('Duplicate Email');
    } 

    
    
    
    
    
    
    
    return context;
  };
}
