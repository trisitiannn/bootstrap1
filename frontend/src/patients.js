//Postal patterns found at  http://html5pattern.com/Postal_Codes



import React, { Component } from 'react';
import axios from 'axios';
class Patients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fname: '',
      lname: '',
      email: '',
      country: 'canada',
      reigon: '',
      postal: '',
      lowPressure: 0,
      highPressure: 1,
      formclass: '',
      postalPattern: '',
      bpclass: 'form-control',
      bpErr: 'enter a valid number',
      postalError: "Enter a valid postal code",
      canadianProvinces: [],
      americanStates: [],
      selectedReigon: ["Select a country"]
    };
    this.handleFname = this.handleFname.bind(this);
    this.handleLname = this.handleLname.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handleCountry = this.handleCountry.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.changeReigon = this.changeReigon.bind(this);
    this.handleReigon = this.handleReigon.bind(this);
    this.handlePostal = this.handlePostal.bind(this);
    this.handleLowPressure = this.handleLowPressure.bind(this);
    this.handleHighPressure = this.handleHighPressure.bind(this);
  }

  handleFname(e) {
    this.setState({ fname: e.target.value });
  }
  handleLname(e) {
    this.setState({ lname: e.target.value });
  }

  handleEmail(e) {
    this.setState({ email: e.target.value });
  }

  handleCountry(e) {
    this.setState({ country: e.target.value });
    this.changeReigon(e.target.value);
  }
  handleReigon(e) {
    this.setState({ reigon: e.target.value });
  }
  handlePostal(e) {
    this.setState({ postal: e.target.value });
  }
  handleLowPressure(e) {
    this.setState({ lowPressure: e.target.value });
  }

  handleHighPressure(e) {
    this.setState({ highPressure: e.target.value });
  }



  changeReigon(value) {
    if (value === "canada") {
      this.setState({
        selectedReigon: this.state.canadianProvinces,
        postalPattern: "[A-Za-z][0-9][A-Za-z] [0-9][A-Za-z][0-9]",
        postalError: "Format is A0A 0A0"
      }, () => {
        console.log(this.state.selectedReigon);
      })
    }
    else if (value === "usa") {
      this.setState({
        selectedReigon: this.state.americanStates,
        postalPattern: "(\d{5}([\-]\d{4})?)",
        postalError: "Format is nnnnn or nnnnn-nnnn"
      }, () => {
        console.log(this.state.selectedReigon);
      })
    }
    else {
      this.setState({ selectedReigon: ["Select a country"] })
    }

  }




  deletePatient(id, ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    const { patientService } = this.props;

    /////////////////////////////////////////////////
    // WRITE THE CODE HERE TO REMOVE THE SELECTED CAR
    // START v
    /////////////////////////////////////////////////

    /////////////////////////////////////////////////
    // END ^
    /////////////////////////////////////////////////
  }

  handleSubmit(ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    const { patientService } = this.props;

    ///////////////////////////////////////
    // WRITE THE CODE HERE TO ADD A NEW CAR
    // START v
    ///////////////////////////////////////
    //const make = country.value;
    if (ev.target.checkValidity()) {
      console.log(`First name ${this.state.fname} \n Last name ${this.state.lname} \n Email ${this.state.email} \n Country ${this.state.country} \n Reigon ${this.state.reigon} \n 
                  Postal ${this.state.postal} \n Low Pressure ${this.state.lowPressure} \nHigh Pressure ${this.state.highPressure}`);
      patientService.create({
          fname: this.state.fname,
          lname: this.state.lname,
          email: this.state.email,
          country: this.state.country,
          reigon: this.state.reigon,
          postal: this.state.postal,
          lowPressure: this.state.lowPressure,
          highPressure: this.state.highPressure
        })
        .then(() => {
          this.setState({
            fname: '',
            lname: '',
            email: '',
            country: 'canada',
            reigon: '',
            postal: '',
            lowPressure: 0,
            highPressure: 1,
            formclass: '',
            postalPattern: '',
            bpclass: 'form-control',
            bpErr: 'enter a valid number',
            postalError: "Enter a valid postal code",
            canadianProvinces: [],
            americanStates: [],
            selectedReigon: ["Select a country"]
          })
        })
        .then(()=>{
          document.getElementById("patienForm").reset(); 
        })
        .catch(err => {
          this.setState({
            formclass: '',
            bpErr: err.message,
            bpclass: 'form-control is-invalid'
          })
        })
    }
    else {
      this.setState({ formclass: 'was-validated' });
    }



    ///////////////////////////////////////
    // END ^
    ///////////////////////////////////////

    ev.preventDefault();
  }
  //populate with provinces and states
  componentDidMount() {
    axios.get('http://geogratis.gc.ca/services/geoname/en/codes/province')
      .then(res => {
        const provArr = res.data.definitions.map(prov => (
          prov.description
        ))
        this.setState({
          canadianProvinces: provArr,
        })
      })

    axios.get('https://gist.githubusercontent.com/mshafrir/2646763/raw/8b0dbb93521f5d6889502305335104218454c2bf/states_titlecase.json')
      .then(other => {
        const stateArr = other.data.map(state => (
          state.name
        ))
        this.setState({
          americanStates: stateArr
        })
      })
  }
  render() {
    const { patients } = this.props;

    return (
      <div>
      <div className="py-5 text-center">
        <h2>Patients</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.handleSubmit} className={this.state.formclass} noValidate id="patienForm">
            <div className="row">
              <div className="col-lg-4 col-md-6 col-sm-12 mb-3">
                <label htmlFor="fname">First name: </label>
                <input type="text" className="form-control" id="fname" defaultValue="" required onChange={this.handleFname} />
                <div className="invalid-feedback">
                    A first name is required
                </div>
              </div>
              
              
              <div className="col-lg-4 col-md-6 col-sm-12 mb-3">
                <label htmlFor="lname">Last name: </label>
                <input type="text" className="form-control" id="lname" defaultValue="" required onChange={this.handleLname} />
                <div className="invalid-feedback">
                    A Last name is required
                </div>
              </div>
              
              
              <div className="col-lg-4 col-md-12 col-sm-12 mb-3">
                <label htmlFor="email">Email: </label>
                <input type="email" className="form-control" id="email" defaultValue="" required onChange={this.handleEmail}/>
                <div className="invalid-feedback">
                    Enter a valid email
                </div>
              </div>
              
              
              <div className="col-lg-4 col-md-6 col-sm-12 mb-3">
                <label htmlFor="country">Select your country </label>
                <select className="form-control" id="country" defaultValue="" required onChange={this.handleCountry} >
                <option value=""> -- select an option -- </option>
                <option value="canada">Canada</option>
                <option value="usa">USA</option>
                </select> 
                <div className="invalid-feedback">
                    Choose your country!
                </div>
              </div>
              
            <div className="col-lg-4 col-md-6 col-sm-12 mb-3">
                <label htmlFor="reigon">Select your reigon</label>
                <select className="form-control" id="reigon" defaultValue="" required onChange={this.handleReigon}>
                {this.state.selectedReigon.map(reigon =>(
                  <option key={reigon} value={reigon} >{reigon}</option>
                ))}
                </select>
              <div className="invalid-feedback">
                Choose your Reigon!
              </div>
            </div>
            
            <div className="col-lg-4 col-md-6 col-sm-12 mb-3">
              <label htmlFor="postal">Enter a postal code</label>
              <input type="text" className="form-control" id="postal" defaultValue="" required onChange={this.handlePostal} pattern={this.state.postalPattern} />
              <div className="invalid-feedback">
                {this.state.postalError}
              </div>
            </div>
            
            <div className="col-lg-6 col-sm-12 mb-3">
              <label htmlFor="lowPressure">Enter your lowest Systolic bp this year</label>
              <input type="number" className={this.state.bpclass} id="lowPressure" defaultValue="0" required onChange={this.handleLowPressure} />
              <div className="invalid-feedback">
                {this.state.bpErr}
              </div>
            </div>
            
            <div className="col-lg-6 col-sm-12 mb-3">
              <label htmlFor="highPressure">Enter your highest Systolic bp this year</label>
              <input type="number" className={this.state.bpclass} id="highPressure" defaultValue="1" required onChange={this.handleHighPressure} />
              <div className="invalid-feedback">
                {this.state.bpErr}
              </div>
            </div>
            
            
            
            </div>
            

            
            
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add patient</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Patients;
